# express-jquery

[![license](https://img.shields.io/badge/license-MIT-green.svg)](https://gitlab.com/pamorana/express-jquery/blob/master/LICENSE)
[![npm](https://img.shields.io/npm/v/@pamorana/express-jquery.svg)](https://www.npmjs.com/package/@pamorana/express-jquery)
[![npm](https://img.shields.io/npm/dt/@pamorana/express-jquery.svg)](https://www.npmjs.com/package/@pamorana/express-jquery)

```javascript
const express = require('express');
const expressJQuery = require('@pamorana/express-jquery');
const app = express();

app.use('/js', expressJQuery({
    full: true,
    slim: true,
}));

app.listen(3000);
```

## Installation
Install express-jquery as well as a jQuery version of your choice.

### Using yarn
```
yarn add @pamorana/express-jquery jquery@latest
```

### Using npm
```
npm install @pamorana/express-jquery jquery@latest --save
```

## Documentation

### Set up an Express app
```javascript
const express = require('express');
const expressJQuery = require('@pamorana/express-jquery');
const app = express();

app.listen(3000);
```

### Options

The default options will serve both the full and slim build of jQuery in their respective minified form.

Option | Description                                                                                                      | Type        | Default
-------|------------------------------------------------------------------------------------------------------------------|-------------|-----------------
`full` | Wheter to serve the default version of jQuery, both unminifed and minified versions.                             | `Boolean`   |
`full` | Same as above but gives control of which veresion should be served. First value is unminifed, second is minfied. | `Boolean[]` | `[false, true]`
`slim` | Wheter to serve the slim version of jQuery, both unminifed and minified versions.                                | `Boolean`   |
`slim` | Same as above but gives control of which veresion should be served. First value is unminifed, second is minfied. | `Boolean[]` | `[false, true]`

### Where are the files served?
Given the topmost example, files will be served directly in the `/js` route:

File                    | Full build                         | Slim build
------------------------|------------------------------------|-----------------------------------------
Unminified              | `example.com/js/jquery.js`         | `example.com/js/jquery.slim.js`
Minified                | `example.com/js/jquery.min.js`     | `example.com/js/jquery.slim.min.js`
Map                     | `example.com/js/jquery.min.map`    | `example.com/js/jquery.slim.min.map`

### Examples

#### Serve all builds
```javascript
app.use(expressJQuery({
    full: true,
    slim: true,
}));
```

#### Serve only minifed bulids
```javascript
app.use(expressJQuery({
    full: [ false, true ],
    slim: [ false, true ],
}));
```

#### Serve only unminifed builds
```javascript
app.use(expressJQuery({
    full: [ true, false ],
    slim: [ true, false ],
}));
```

#### Serve only full build
```javascript
app.use(expressJQuery({
    full: true,
}));
```

#### Serve only slim build
```javascript
app.use(expressJQuery({
    slim: true,
}));
```

## Contributing
File an [issue](https://gitlab.com/pamorana/express-jquery/issues) or even better create a [merge request](https://gitlab.com/pamorana/express-jquery/merge_requests).


## License
[MIT](LICENSE) &copy; [Pamorana](https://gitlab.com/Pamorana) and [contributors](https://gitlab.com/pamorana/express-jquery/graphs/master).
