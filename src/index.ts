import { join as joinPath, parse as parsePath } from 'path'
import { Router } from 'express'

export namespace ExpressJQuery {
    export const jQueryDistFolder: string = parsePath(require.resolve('jquery')).dir

    /**
     * @typedef {object} IOptions
     * @property {(boolean | Array<boolean>)} [full] - Whether to use the default build of jQuery.
     * @property {(boolean | Array<boolean>)} [slim] - Whether to use the slim build of jQuery.
     */
    export interface IOptions {
        full?: boolean | Array<boolean>,
        slim?: boolean | Array<boolean>,
    }

    /**
     * @typedef {object} IDefaultOptions
     * @property {Array<boolean>)} full - Whether to use the default build of jQuery.
     * @property {Array<boolean>)} slim - Whether to use the slim build of jQuery.
     */
    export interface IDefaultOptions extends IOptions {
        full: Array<boolean>,
        slim: Array<boolean>,
    }

    /**
     * Default options for express-jquery middleware.
     * @constant
     * @type {IDefaultOptions}
     * @default
     */
    export const defaultOptions: IDefaultOptions = {
        full: [ false, true ],
        slim: [ false, true ],
    }
}

class ExpressJQueryMiddleware {
    public router: Router

    public jQueryFull: boolean
    public jQueryMinifiedFull: boolean

    public jQuerySlim: boolean
    public jQueryMinifiedSlim: boolean

    constructor(private options: ExpressJQuery.IOptions = ExpressJQuery.defaultOptions) {
        this.router = Router()

        this.validateOptions().handleFullBuildOptions().handleSlimBuildOptions()
    }

    /**
     * Wheter the options passed in enables any build of jQuery to be served.
     * @return {boolean}
     */
    public isJQueryServed(): boolean {
        // Emit a warning if all versions are disabled
        if (this.jQueryFull !== true && this.jQueryMinifiedFull !== true && this.jQuerySlim !== true && this.jQueryMinifiedSlim !== true) {
            process.emitWarning('All versions of jQuery are disabled.', 'ExpressJQueryWarning')

            return false
        }

        return true
    }

    /**
     * Adds routes for jQuery's full build to the router if options enable it.
     * @return {ExpressJQueryMiddleware}
     */
    public addJQueryFullRoutes(): ExpressJQueryMiddleware {
        if (this.jQueryFull === false) {
            return this
        }

        this.router.use('/jquery.js', (req, res) => {
            const file: string = joinPath(ExpressJQuery.jQueryDistFolder, 'jquery.js')
            res.sendFile(file)
        })

        return this
    }

    /**
     * Adds routes for jQuery's minified full build to the router if options enable it.
     * @return {ExpressJQueryMiddleware}
     */
    public addJQueryMinifiedFullRoutes(): ExpressJQueryMiddleware {
        if (this.jQueryMinifiedFull === false) {
            return this
        }

        this.router.use('/jquery.min.js', (req, res) => {
            const file: string = joinPath(ExpressJQuery.jQueryDistFolder, 'jquery.min.js')
            res.sendFile(file)
        })

        this.router.use('/jquery.min.map', (req, res) => {
            const file: string = joinPath(ExpressJQuery.jQueryDistFolder, 'jquery.min.map')
            res.sendFile(file)
        })

        return this
    }

    /**
     * Adds routes for jQuery's slim build to the router if options enable it.
     * @return {ExpressJQueryMiddleware}
     */
    public addJQuerySlimRoutes(): ExpressJQueryMiddleware {
        if (this.jQuerySlim === false) {
            return this
        }

        this.router.use('/jquery.slim.js', (req, res) => {
            const file: string = joinPath(ExpressJQuery.jQueryDistFolder, 'jquery.slim.js')
            res.sendFile(file)
        })

        return this
    }

    /**
     * Adds routes for jQuery's minified slim build to the router if options enable it.
     * @return {ExpressJQueryMiddleware}
     */
    public addJQueryMinifiedSlimRoutes(): ExpressJQueryMiddleware {
        if (this.jQueryMinifiedSlim === false) {
            return this
        }

        this.router.use('/jquery.slim.min.js', (req, res) => {
            const file: string = joinPath(ExpressJQuery.jQueryDistFolder, 'jquery.slim.min.js')
            res.sendFile(file)
        })

        this.router.use('/jquery.slim.min.map', (req, res) => {
            const file: string = joinPath(ExpressJQuery.jQueryDistFolder, 'jquery.slim.min.map')
            res.sendFile(file)
        })

        return this
    }

    /**
     * Validate options passed in to the constructor.
     * @return {ExpressJQueryMiddleware}
     */
    private validateOptions(): ExpressJQueryMiddleware {
        // Emit a warning if options is not valid
        if (typeof this.options !== 'object') {
            process.emitWarning('options is not an object. Using default options.', 'ExpressJQueryWarning')
            this.options = ExpressJQuery.defaultOptions
        }

        return this
    }

    /**
     * Parse options for jQuery's full builds.
     * @return {ExpressJQueryMiddleware}
     */
    private handleFullBuildOptions(): ExpressJQueryMiddleware {
        // Handle jQuery full build options
        if (this.options.hasOwnProperty('full') === false && this.options.hasOwnProperty('slim') === false) {
            this.options.full = ExpressJQuery.defaultOptions.full
        } else if (this.options.hasOwnProperty('full') === false) {
            this.options.full = false
        }

        if (this.options.full instanceof Array) {
            [ this.jQueryFull, this.jQueryMinifiedFull ] = this.options.full as boolean[]
        } else if (typeof this.options.full === 'boolean') {
            this.jQueryFull = this.options.full as boolean
            this.jQueryMinifiedFull = this.options.full as boolean
        }

        return this
    }

    /**
     * Parse options for jQuery's slim builds.
     * @return {ExpressJQueryMiddleware}
     */
    private handleSlimBuildOptions(): ExpressJQueryMiddleware {
        // Handle jQuery slim build options
        if (this.options.hasOwnProperty('slim') === false && this.options.hasOwnProperty('full') === false) {
            this.options.slim = ExpressJQuery.defaultOptions.slim
        } else if (this.options.hasOwnProperty('slim') === false) {
            this.options.slim = false
        }

        if (this.options.slim instanceof Array) {
            [ this.jQuerySlim, this.jQueryMinifiedSlim ] = this.options.slim as boolean[]
        } else if (typeof this.options.slim === 'boolean') {
            this.jQuerySlim = this.options.slim as boolean
            this.jQueryMinifiedSlim = this.options.slim as boolean
        }

        return this
    }
}

/**
 * The exported function used to host jQuery with an Express application.
 *
 * @param {ExpressJQuery.IOptions} [options] - Middleware configuration object.
 * @return {Router} - An Express router to host jQuery.
 */
const expressJQuery = (options: ExpressJQuery.IOptions = ExpressJQuery.defaultOptions): Router => {
    const expressJQueryMiddleware: ExpressJQueryMiddleware = new ExpressJQueryMiddleware(options)

    if (expressJQueryMiddleware.isJQueryServed()) {
        expressJQueryMiddleware.addJQueryFullRoutes()
            .addJQueryMinifiedFullRoutes()
            .addJQuerySlimRoutes()
            .addJQueryMinifiedSlimRoutes()
    }

    return expressJQueryMiddleware.router
}

export default expressJQuery
