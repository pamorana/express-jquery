"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const express_1 = require("express");
var ExpressJQuery;
(function (ExpressJQuery) {
    ExpressJQuery.jQueryDistFolder = path_1.parse(require.resolve('jquery')).dir;
    ExpressJQuery.defaultOptions = {
        full: [false, true],
        slim: [false, true],
    };
})(ExpressJQuery = exports.ExpressJQuery || (exports.ExpressJQuery = {}));
class ExpressJQueryMiddleware {
    constructor(options = ExpressJQuery.defaultOptions) {
        this.options = options;
        this.router = express_1.Router();
        this.validateOptions().handleFullBuildOptions().handleSlimBuildOptions();
    }
    isJQueryServed() {
        if (this.jQueryFull !== true && this.jQueryMinifiedFull !== true && this.jQuerySlim !== true && this.jQueryMinifiedSlim !== true) {
            process.emitWarning('All versions of jQuery are disabled.', 'ExpressJQueryWarning');
            return false;
        }
        return true;
    }
    addJQueryFullRoutes() {
        if (this.jQueryFull === false) {
            return this;
        }
        this.router.use('/jquery.js', (req, res) => {
            const file = path_1.join(ExpressJQuery.jQueryDistFolder, 'jquery.js');
            res.sendFile(file);
        });
        return this;
    }
    addJQueryMinifiedFullRoutes() {
        if (this.jQueryMinifiedFull === false) {
            return this;
        }
        this.router.use('/jquery.min.js', (req, res) => {
            const file = path_1.join(ExpressJQuery.jQueryDistFolder, 'jquery.min.js');
            res.sendFile(file);
        });
        this.router.use('/jquery.min.map', (req, res) => {
            const file = path_1.join(ExpressJQuery.jQueryDistFolder, 'jquery.min.map');
            res.sendFile(file);
        });
        return this;
    }
    addJQuerySlimRoutes() {
        if (this.jQuerySlim === false) {
            return this;
        }
        this.router.use('/jquery.slim.js', (req, res) => {
            const file = path_1.join(ExpressJQuery.jQueryDistFolder, 'jquery.slim.js');
            res.sendFile(file);
        });
        return this;
    }
    addJQueryMinifiedSlimRoutes() {
        if (this.jQueryMinifiedSlim === false) {
            return this;
        }
        this.router.use('/jquery.slim.min.js', (req, res) => {
            const file = path_1.join(ExpressJQuery.jQueryDistFolder, 'jquery.slim.min.js');
            res.sendFile(file);
        });
        this.router.use('/jquery.slim.min.map', (req, res) => {
            const file = path_1.join(ExpressJQuery.jQueryDistFolder, 'jquery.slim.min.map');
            res.sendFile(file);
        });
        return this;
    }
    validateOptions() {
        if (typeof this.options !== 'object') {
            process.emitWarning('options is not an object. Using default options.', 'ExpressJQueryWarning');
            this.options = ExpressJQuery.defaultOptions;
        }
        return this;
    }
    handleFullBuildOptions() {
        if (this.options.hasOwnProperty('full') === false && this.options.hasOwnProperty('slim') === false) {
            this.options.full = ExpressJQuery.defaultOptions.full;
        }
        else if (this.options.hasOwnProperty('full') === false) {
            this.options.full = false;
        }
        if (this.options.full instanceof Array) {
            [this.jQueryFull, this.jQueryMinifiedFull] = this.options.full;
        }
        else if (typeof this.options.full === 'boolean') {
            this.jQueryFull = this.options.full;
            this.jQueryMinifiedFull = this.options.full;
        }
        return this;
    }
    handleSlimBuildOptions() {
        if (this.options.hasOwnProperty('slim') === false && this.options.hasOwnProperty('full') === false) {
            this.options.slim = ExpressJQuery.defaultOptions.slim;
        }
        else if (this.options.hasOwnProperty('slim') === false) {
            this.options.slim = false;
        }
        if (this.options.slim instanceof Array) {
            [this.jQuerySlim, this.jQueryMinifiedSlim] = this.options.slim;
        }
        else if (typeof this.options.slim === 'boolean') {
            this.jQuerySlim = this.options.slim;
            this.jQueryMinifiedSlim = this.options.slim;
        }
        return this;
    }
}
const expressJQuery = (options = ExpressJQuery.defaultOptions) => {
    const expressJQueryMiddleware = new ExpressJQueryMiddleware(options);
    if (expressJQueryMiddleware.isJQueryServed()) {
        expressJQueryMiddleware.addJQueryFullRoutes()
            .addJQueryMinifiedFullRoutes()
            .addJQuerySlimRoutes()
            .addJQueryMinifiedSlimRoutes();
    }
    return expressJQueryMiddleware.router;
};
exports.default = expressJQuery;
