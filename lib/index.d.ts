/// <reference types="express" />
import { Router } from 'express';
export declare namespace ExpressJQuery {
    const jQueryDistFolder: string;
    interface IOptions {
        full?: boolean | Array<boolean>;
        slim?: boolean | Array<boolean>;
    }
    interface IDefaultOptions extends IOptions {
        full: Array<boolean>;
        slim: Array<boolean>;
    }
    const defaultOptions: IDefaultOptions;
}
declare const expressJQuery: (options?: ExpressJQuery.IOptions) => Router;
export default expressJQuery;
